package com.javainuse.service;

import java.util.List;

import com.javamysql.model.Employee;

public interface EmployeeService {
	List<Employee> getAllEmployees();
}
