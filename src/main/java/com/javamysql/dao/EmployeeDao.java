package com.javamysql.dao;

import java.util.List;

import com.javamysql.model.Employee;

public interface EmployeeDao {
	List<Employee> getAllEmployees();
}
